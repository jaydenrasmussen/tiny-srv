# tiny-srv

A simple way to write micro-services.

### Getting it running ~

fastest way to get a server running is with these two lines:

```javascript
const tiny = require('tiny-srv');
module.exports = tiny().catch(console.log);
```

### Configuring the server

This module returns a promise with the resolved express app object so you can easily hang your routes and middleware onto the app as needed after the module has been initialized. Now, there are a number of configurable options.

```javascript
const tiny = require('tiny-srv');	
module.exports = tiny({
	// defaults to either the environment variable PORT or 8080
    port: 3000
    /* If you wish to use an HTTPS server, you must also pass in a
     * certificate with the server, this should look like
     * certificate : { 
     * 	 certificate: <path to the file>, passphrase: <pass> 
     * }
     */
    enableHTTPS: false,
    /* any of these settings can be set by a boolean value */
    enableCORS: false,
    enableCompression: true,
    enableSecurity: true
})
```

