const express = require("express");
const helmet = require("helmet");
const compress = require("compression");
const cors = require("cors");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

const { pipe } = require("./util");

const pipeline = pipe(validateConfig, expressSettings, expressMiddleware);

module.exports = args => pipeline(args);

function validateConfig(config) {
    const pkg = require(`${process.cwd()}/package.json`);
    if (typeof config !== "object") config = Object();
    config = Object.assign(
        {
            port: process.env.PORT || 8080,
            serviceName: pkg.name,
            enableHTTPS: false,
            enableCORS: false,
            enableCompression: true,
            enableSecurity: true
        },
        config
    );
    return { app: express(), config };
}
function expressSettings(args) {
    let { app, config } = args;
    app.set("port", config.port);
    // more settings here ~
    if (config.title) app.set("title", config.title);

    return { app, config };
}
function expressMiddleware(args) {
    let { app, config } = args;
    // standards
    app.use(bodyParser.json());
    app.use(bodyParser.raw());
    app.use(bodyParser.urlencoded());
    app.use(cookieParser());
    // options
    if (config.enableCORS) app.use(cors());
    if (config.enableCompression) app.use(compress());
    if (config.enableSecurity) app.use(helmet());

    return { app, config };
}
