const http = require('http');
const https = require('https');
const log = require('lumberr');

module.exports = args => {
    let { app, config } = args;
    return config.enableHTTPS && config.certificate
        ? createHTTPSServer(args)
        : createHTTPServer(args);
}
function createHTTPServer(args) {
    let { app, config } = args;
    let server = https.createServer(app);
    server['port'] = config.port;
    server['isHTTPS'] = false;
    server['serviceName'] = config.serviceName;
    server.on('listening', _listening);
    server.listen(config.port);
    return app;
}
function createHTTPSServer(args) {
    let { app, config } = args;
    let server = https.createServer(config.certificate, app);
    server['port'] = config.port;
    server['isHTTPS'] = true;
    server['serviceName'] = config.serviceName;
    server.on('listening', _listening);
    return server;
}
function _listening() {
    return log.success(
        `Express server is running on ${
            this.isHTTPS ? 'https://' : 'http://'
        }localhost:${this.port}/${this.serviceName}`
    );
}
