'use strict';

const { pipe } = require('./lib/util');
const express = require('./lib/express.init');
const server = require('./lib/server.init');

const init = pipe(express, server);
module.exports = async config => init(config);
