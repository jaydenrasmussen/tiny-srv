const test = require('ava');
const mod = require('../');

const AsyncFunction = (async () => {}).constructor;
const GeneratorFunction = function*() {}.constructor;

test('Module exports a promise', t => t.true(mod instanceof AsyncFunction));
test('Util exports a util function', t => {
    const util = require('../lib/util');
    t.true(util.hasOwnProperty('pipe'));
    t.true(Object.prototype.toString.call(util.pipe) === '[object Function]');
});
test('express init exports a function', t => {
    const express = require('../lib/express.init');
    t.true(Object.prototype.toString.call(express) === '[object Function]');
});
test('server init exports a function', t => {
    const server = require('../lib/server.init');
    t.true(Object.prototype.toString.call(server) === '[object Function]');
});
